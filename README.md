# MoviesApp Application

###### 06 / 09 / 2020

- Mobile application developed with React Native for the technical test with NativApps.

### Features

- It has a search bar that will show a maximum of 50 movie records in case the results are found.
- The search is based in movies's names.
- It will show the suggested results found as a list of movies.
- The result found in the suggested list is clickable to see the desired movie in detail.
- Connected to the api of http://www.omdbapi.com/

### Technologies / Tools

- React Native
- Styled Components
- Redux
- React Navigation

### Run the project locally

- npx react-native start
- To run with an android emulator ---> npx react-native run-android
- To run with an ios simulator ---> npx react-native run-ios

##### Developed by: Meggie Sánchez
