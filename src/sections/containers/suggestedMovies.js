import React from 'react';
import {FlatList} from 'react-native';
import {useSelector} from 'react-redux';
import {Suggestion} from '../components/suggestion';
import {Empty} from '../components/empty';

export const SuggestedMovies = (navigation) => {
  const suggestedMovies = useSelector((state) => state.suggestedMovies);

  const renderItem = (movie) => (
    <Suggestion {...movie} onPress={() => viewMovieDetail(movie)} />
  );

  const keyExtractor = (item) => item['imdbID'];

  const renderEmpty = () => (
    <Empty text="There are no suggestions for what you are looking for" />
  );

  const viewMovieDetail = (movie) => {
    navigation.navigate('Movie', {movieId: movie.item.imdbID});
  };

  return (
    <>
      <FlatList
        data={suggestedMovies}
        renderItem={renderItem}
        renderEmpty={renderEmpty}
        ListEmptyComponent={renderEmpty}
        keyExtractor={keyExtractor}
      />
    </>
  );
};
