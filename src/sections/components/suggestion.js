import React from 'react';
import {
  Title,
  Left,
  Right,
  Cover,
  Year,
  CoverEmpty,
  TouchableMovie,
} from './suggestionStyles';

export const Suggestion = ({item, onPress}) => {
  return (
    <TouchableMovie onPress={onPress}>
      <Left>
        {item.Poster !== 'N/A' ? (
          <Cover resizeMode="contain" source={{uri: item.Poster}}></Cover>
        ) : (
          <CoverEmpty />
        )}
      </Left>
      <Right>
        <Title>{item.Title}</Title>
        <Year>{item.Year}</Year>
      </Right>
    </TouchableMovie>
  );
};
