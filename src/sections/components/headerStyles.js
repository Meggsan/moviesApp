import styled, {css} from 'styled-components/native';
import {Platform} from 'react-native';

export const Logo = styled.Image`
  width: 117px;
  height: 21.45px;
`;

export const SafeArea = styled.SafeAreaView`
  ${Platform.select({
    ios: css`
      flex: 1;
      justify-content: center;
      flex-direction: row;
      align-items: center;
    `,
  })};
`;
