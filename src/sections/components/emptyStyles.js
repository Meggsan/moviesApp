import styled from 'styled-components/native';

export const Container = styled.View`
  margin: 15px;
  flex-direction: row;
  justify-content: center;
`;

export const Text = styled.Text`
  font-size: 16px;
  text-align: center;
`;
