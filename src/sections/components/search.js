import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {TouchableOpacity, Platform} from 'react-native';
import Autocomplete from 'react-native-autocomplete-input';
import {SuggestedMovies} from '../containers/suggestedMovies';
import {
  Container,
  ContainerMovies,
  ContainerInput,
  ItemText,
  InfoText,
  Styles,
} from './searchStyles';
import {getMovies} from '../../utils/api';

export const SearchLayout = (navigation) => {
  const [isFocused, setFocused] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [firstSearch, setFirstSearch] = useState(false);
  const dispatch = useDispatch();
  const suggestedMovies = useSelector((state) => state.suggestedMovies);

  useEffect(() => {
    if (searchText.length >= 3) {
      setFirstSearch(true);
      dispatch({
        type: 'SET_SEARCH_AGAIN',
      });
      for (let i = 1; i <= 5; i++) {
        searchMovies(searchText, i);
      }
    }
  }, [searchText]);

  const searchMovies = async (text, i) => {
    const movies = await getMovies(text, i);
    if (movies.Response === 'True') {
      dispatch({
        type: 'SET_SUGGESTED_MOVIES',
        payload: {movies: movies.Search},
      });
    }
  };

  const handleChangeText = (text) => {
    let findText = text.trim();
    if (findText !== searchText) setSearchText(findText);
  };

  const keyExtractor = (item) => item['imdbID'];

  const viewMovieDetail = (movie) => {
    navigation.navigate('Movie', {movieId: movie.imdbID});
    setFocused(false);
  };

  const onFocus = () => setFocused(false);
  const onBlur = () => setFocused(true);

  const renderItem = ({item}) => (
    <TouchableOpacity key={item.imdbID} onPress={() => viewMovieDetail(item)}>
      <ItemText>{item.Title}</ItemText>
    </TouchableOpacity>
  );

  return (
    <Container>
      {Platform.OS === 'ios' ? (
        <Autocomplete
          listStyle={Styles.listStyle}
          inputContainerStyle={Styles.inputContainerStyle}
          autoCapitalize="none"
          autoCorrect={false}
          data={suggestedMovies}
          defaultValue={''}
          onChangeText={handleChangeText}
          placeholder="Enter your favorite movie by name"
          hideResults={isFocused}
          keyExtractor={keyExtractor}
          onFocus={onFocus}
          onBlur={onBlur}
          renderItem={renderItem}
        />
      ) : (
        <ContainerInput>
          <Autocomplete
            listStyle={Styles.listStyle}
            inputContainerStyle={Styles.inputContainerStyle}
            autoCapitalize="none"
            autoCorrect={false}
            data={suggestedMovies}
            defaultValue={''}
            onChangeText={handleChangeText}
            placeholder="Enter your favorite movie by name"
            hideResults={isFocused}
            keyExtractor={keyExtractor}
            onFocus={onFocus}
            onBlur={onBlur}
            renderItem={renderItem}
          />
        </ContainerInput>
      )}

      <ContainerMovies>
        {suggestedMovies.length >= 0 && firstSearch ? (
          <SuggestedMovies {...navigation} />
        ) : (
          <InfoText>
            Your results will appear here, do your search with at least 3
            characters.
          </InfoText>
        )}
      </ContainerMovies>
    </Container>
  );
};
