import styled, {css} from 'styled-components/native';
import {StyleSheet, Platform} from 'react-native';
import {SECONDARY_COLOR} from '../../styles/colors';

export const Container = styled.View`
  flex: 1;
  margin: 10px 10px 0px 10px;
`;

export const ContainerInput = styled.View`
  ${Platform.select({
    android: css`
      flex: 1;
      left: 0;
      position: absolute;
      right: 0;
      top: 0px;
      z-index: 1;
    `,
  })};
`;

export const ContainerMovies = styled.View`
  ${Platform.select({
    android: css`
      margin: 45px 0px 20px 0px;
    `,
    ios: css`
      margin: 5px 0px 20px 0px;
    `,
  })};
  flex: 1;
`;

export const ItemText = styled.Text`
  font-size: 15px;
  padding: 5px;
  margin: 2px;
  font-family: 'Lato';
`;

export const InfoText = styled.Text`
  font-family: 'Lato';
  text-align: center;
  font-size: 16px;
  margin-top: 5px;
`;

export const Styles = StyleSheet.create({
  inputContainerStyle: {
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: SECONDARY_COLOR,
    borderRadius: 7,
  },
  listStyle: {
    maxHeight: 350,
  },
});
