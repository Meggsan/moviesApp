import styled from 'styled-components/native';
import {
  PRIMARY_COLOR,
  BLACK_COLOR,
  GRAY_COLOR,
  SECONDARY_COLOR,
  WHITE_COLOR,
} from '../../styles/colors';

export const TouchableMovie = styled.TouchableOpacity`
  flex-direction: row;
  margin: 4px 0px;
  border: 1px ${GRAY_COLOR};
  border-radius: 7px;
  background-color: ${PRIMARY_COLOR};
`;

export const Left = styled.View`
  background-color: ${PRIMARY_COLOR};
  height: 150px;
  width: 100px;
`;

export const Right = styled.View`
  padding: 0px 15px;
  justify-content: space-around;
  flex-shrink: 1;
`;

export const Cover = styled.Image`
  height: 150px;
  width: 100px;
  border-top-left-radius: 7px;
  border-bottom-left-radius: 7px;
`;

export const CoverEmpty = styled.View`
  height: 150px;
  width: 100px;
  background-color: ${BLACK_COLOR};
  border-top-left-radius: 7px;
  border-bottom-left-radius: 7px;
`;

export const Title = styled.Text`
  font-size: 18px;
  color: ${WHITE_COLOR};
  font-family: 'Lato';
`;

export const Year = styled.Text`
  background-color: ${SECONDARY_COLOR};
  padding: 4px 6px;
  color: ${WHITE_COLOR};
  font-size: 11px;
  border-radius: 7px;
  overflow: hidden;
  align-self: flex-start;
  font-family: 'Lato';
`;
