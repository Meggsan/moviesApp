import React from 'react';
import {Logo, SafeArea} from './headerStyles';

export const Header = () => {
  return (
    <>
      <SafeArea>
        <Logo
          resizeMode="contain"
          source={{
            uri:
              'https://nativapps.com/wp-content/uploads/2019/06/logo-nativapps-1.png',
          }}
        />
      </SafeArea>
    </>
  );
};
