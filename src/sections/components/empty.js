import React from 'react';
import {Container, Text} from './emptyStyles';

export const Empty = ({text}) => {
  return (
    <Container>
      <Text>{text}</Text>
    </Container>
  );
};
