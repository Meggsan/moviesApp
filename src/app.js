import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {Home} from './screens/containers/home';
import {Movie} from './screens/containers/movie';
import {Header} from './sections/components/header';
import {PRIMARY_COLOR, WHITE_COLOR} from './styles/colors';

const Stack = createStackNavigator();

const options = {
  headerTitle: (props) => <Header {...props} />,
  headerStyle: {
    backgroundColor: PRIMARY_COLOR,
  },
  headerTintColor: WHITE_COLOR,
};

export const AppLayout = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="Home" component={Home} options={options} />
      <Stack.Screen name="Movie" component={Movie} options={options} />
    </Stack.Navigator>
  );
};
