// const API_KEY = '5eec5adc';
const API_KEY = '1ecd07c5';
const BASE_API = `http://www.omdbapi.com/`;

export const getMovies = async (name, i) => {
  const query = await fetch(
    `${BASE_API}?apikey=${API_KEY}&s=${name}&page=${i}`,
  );
  const data = await query.json();
  return data;
};

export const getMovieDetail = async (id) => {
  const query = await fetch(`${BASE_API}?&apikey=${API_KEY}&i=${id}`);
  const data = await query.json();
  return data;
};
