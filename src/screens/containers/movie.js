import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {MovieLayout} from '../components/movie';
import {getMovieDetail} from '../../utils/api';

export const Movie = ({route}) => {
  const {movieId} = route.params;
  const movie = useSelector((state) => state.movie);
  const dispatch = useDispatch();

  useEffect(() => {
    getMovie();
  }, []);

  const getMovie = async () => {
    const movie = await getMovieDetail(movieId);
    dispatch({type: 'SET_MOVIE', payload: {movie: movie}});
  };

  return <MovieLayout {...movie} />;
};
