import React from 'react';
import {SearchLayout} from '../../sections/components/search';

export const Home = ({navigation}) => {
  return (
    <>
      <SearchLayout {...navigation} />
    </>
  );
};
