import styled from 'styled-components/native';
import {
  PRIMARY_COLOR,
  WHITE_COLOR,
  SECONDARY_COLOR,
  BLACK_COLOR,
} from '../../styles/colors';

export const Container = styled.View`
  margin: 15px;
  flex-direction: row;
  justify-content: center;
`;

export const Text = styled.Text`
  font-size: 16px;
  font-family: 'Lato';
`;

export const TextDescription = styled.Text`
  font-size: 16px;
  color: ${WHITE_COLOR};
  font-family: 'Lato';
`;

export const Cover = styled.Image`
  height: 150px;
  width: 100px;
`;

export const CoverEmpty = styled.View`
  height: 200px;
  width: 100%;
  background-color: ${BLACK_COLOR};
`;

export const ContainerCover = styled.ImageBackground`
  width: 100%;
  padding: 25px;
`;

export const Overlay = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-color: 'rgba(0,0,0,0.5)';
  height: 200px;
`;

export const TitleText = styled.Text`
  font-size: 25px;
  text-align: center;
  font-family: 'Lato-Black';
  margin-bottom: 5px;
`;

export const Year = styled.Text`
  font-size: 18px;
  font-family: 'Lato';
`;

export const ContainerTitle = styled.View`
  align-items: center;
  padding: 10px 10px;
  flex-shrink: 1;
`;

export const ContainerDescription = styled.View`
  padding: 10px 15px;
  margin-bottom: 10px;
`;

export const ContainerInfo = styled.View`
  flex-direction: row;
  padding: 10px 15px;
  justify-content: center;
  align-items: center;
  background-color: ${SECONDARY_COLOR};
  flex-wrap: wrap;
  margin-bottom: 5px;
`;

export const DescriptionText = styled.Text`
  font-size: 20px;
  font-family: 'Lato-Black';
  color: ${WHITE_COLOR};
`;

export const TextInfo = styled.Text`
  color: ${WHITE_COLOR};
  font-family: 'Lato';
`;

export const Bullet = styled.Text`
  color: ${WHITE_COLOR};
  font-size: 5px;
  font-family: 'Lato';
`;

export const ContainerImdb = styled.View`
  flex-direction: row;
  justify-content: space-around;
  margin-bottom: 10px;
`;

export const ImdbInfo = styled.View`
  flex-direction: row;
`;

export const Imdb = styled.Text`
  background-color: ${SECONDARY_COLOR};
  padding: 4px 6px;
  color: ${WHITE_COLOR};
  font-size: 12px;
  border-radius: 7px;
  overflow: hidden;
  font-family: 'Lato';
`;

export const ContainerOtherInfo = styled.View`
  flex-direction: row;
  margin: 0px 15px 5px 15px;
`;

export const OtherInfo = styled.View`
  flex: 0.5;
  margin-bottom: 10px;
`;

export const OtherInfoTitle = styled.Text`
  font-family: 'Lato-Black';
  color: ${WHITE_COLOR};
`;
export const OtherInfoText = styled.Text`
  color: ${WHITE_COLOR};
  font-family: 'Lato';
`;

export const ContainerBottom = styled.View`
  background-color: ${PRIMARY_COLOR};
  border-radius: 7px;
  margin: 10px 15px 15px 15px;
`;
