import React from 'react';
import {ScrollView} from 'react-native';
import {
  Cover,
  ContainerCover,
  Overlay,
  Text,
  TitleText,
  ContainerTitle,
  ContainerDescription,
  DescriptionText,
  TextDescription,
  ContainerInfo,
  TextInfo,
  Bullet,
  ContainerImdb,
  Imdb,
  ImdbInfo,
  OtherInfoTitle,
  OtherInfoText,
  OtherInfo,
  ContainerOtherInfo,
  ContainerBottom,
  CoverEmpty,
} from './movieStyles';

export const MovieLayout = ({
  Released,
  Title,
  Runtime,
  Country,
  Genre,
  Director,
  Actors,
  imdbRating,
  imdbVotes,
  Awards,
  Poster,
  Plot,
  Language,
}) => {
  return (
    <ScrollView>
      {Poster !== 'N/A' ? (
        <ContainerCover source={{uri: Poster}} resizeMode="cover">
          <Overlay></Overlay>
          <Cover resizeMode="contain" source={{uri: Poster}}></Cover>
        </ContainerCover>
      ) : (
        <CoverEmpty />
      )}
      <ContainerInfo>
        <TextInfo>{Country} </TextInfo>
        <Bullet>{'\u2B24'}</Bullet>
        <TextInfo> {Runtime} </TextInfo>
        <Bullet>{'\u2B24'}</Bullet>
        <TextInfo> {Genre}</TextInfo>
      </ContainerInfo>
      <ContainerTitle>
        <TitleText>{Title}</TitleText>
        <Text> ({Released})</Text>
      </ContainerTitle>
      <ContainerImdb>
        <ImdbInfo>
          <Text>Rating: </Text>
          <Imdb>{imdbRating}</Imdb>
        </ImdbInfo>
        <ImdbInfo>
          <Text>Votes: </Text>
          <Imdb>{imdbVotes}</Imdb>
        </ImdbInfo>
      </ContainerImdb>
      <ContainerBottom>
        <ContainerDescription>
          <DescriptionText>Description</DescriptionText>
          <TextDescription>{Plot}</TextDescription>
        </ContainerDescription>
        <ContainerOtherInfo>
          <OtherInfo>
            <OtherInfoTitle>Director</OtherInfoTitle>
            <OtherInfoText>{Director}</OtherInfoText>
          </OtherInfo>
          <OtherInfo>
            <OtherInfoTitle>Actors</OtherInfoTitle>
            <OtherInfoText>{Actors}</OtherInfoText>
          </OtherInfo>
        </ContainerOtherInfo>
        <ContainerOtherInfo>
          <OtherInfo>
            <OtherInfoTitle>Awards</OtherInfoTitle>
            <OtherInfoText>{Awards}</OtherInfoText>
          </OtherInfo>
          <OtherInfo>
            <OtherInfoTitle>Language</OtherInfoTitle>
            <OtherInfoText>{Language}</OtherInfoText>
          </OtherInfo>
        </ContainerOtherInfo>
      </ContainerBottom>
    </ScrollView>
  );
};
