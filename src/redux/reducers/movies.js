const initialState = {
  suggestedMovies: [],
  movie: {},
};

export default function movies(state = initialState, action) {
  switch (action.type) {
    case 'SET_SUGGESTED_MOVIES': {
      return {
        ...state,
        suggestedMovies: [...state.suggestedMovies, ...action.payload.movies],
      };
    }

    case 'SET_MOVIE': {
      return {...state, movie: action.payload.movie};
    }

    case 'SET_SEARCH_AGAIN': {
      return {
        ...state,
        suggestedMovies: [],
      };
    }

    default:
      return state;
  }
}
