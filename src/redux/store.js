import {createStore} from 'redux';
import reducer from '../redux/reducers/movies';

const store = createStore(reducer);

export {store};
