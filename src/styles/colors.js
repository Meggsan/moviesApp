export const PRIMARY_COLOR = '#090043';
export const SECONDARY_COLOR = '#FF6F12';
export const WHITE_COLOR = '#FFFFFF';
export const BLACK_COLOR = '#000000';
export const GRAY_COLOR = '#eaeaea';

// export default {
//   primaryColor: '#090043',
//   secondaryColor: '#FF6F12',
//   whiteColor: '#FFFFFF',
//   blackColor: '#000000'
// };
