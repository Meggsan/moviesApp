import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';

import {Provider} from 'react-redux';
import {store} from './src/redux/store';
import {AppLayout} from './src/app';

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <AppLayout />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
